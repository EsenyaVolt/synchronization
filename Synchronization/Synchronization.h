﻿
// Synchronization.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CSynchronizationApp:
// Сведения о реализации этого класса: Synchronization.cpp
//

class CSynchronizationApp : public CWinApp
{
public:
	CSynchronizationApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CSynchronizationApp theApp;
