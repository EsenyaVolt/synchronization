﻿
// SynchronizationDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Synchronization.h"
#include "SynchronizationDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CSynchronizationDlg



CSynchronizationDlg::CSynchronizationDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SYNCHRONIZATION_DIALOG, pParent)
	, message(_T("Введите сообщение"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSynchronizationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE, message);
}

BEGIN_MESSAGE_MAP(CSynchronizationDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_MESSAGE, &CSynchronizationDlg::OnEnChangeMessage)
	ON_BN_CLICKED(IDC_CANCEL, &CSynchronizationDlg::OnBnClickedCancel)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

void CSynchronizationDlg::OnEnChangeMessage()
{
	if (work)
	{
		WriteMessage();
		PulseEvent(handlemass[2]);
	}
}

void CSynchronizationDlg::WriteMessage()
{
	if (lpBaseAddress != NULL)
		GetDlgItem(IDC_MESSAGE)->GetWindowText((PTSTR)lpBaseAddress, 2048);
}

void CSynchronizationDlg::ReadMessage()
{
	if (lpBaseAddress != NULL)
		GetDlgItem(IDC_MESSAGE)->SetWindowText((PTSTR)lpBaseAddress);
}

DWORD WINAPI MyProc(PVOID pvParam)
{
	CSynchronizationDlg* pr = (CSynchronizationDlg*)pvParam;
	while (true)
	{
		DWORD dwrd = WaitForMultipleObjects(3, pr->handlemass, false, INFINITE);
		switch (dwrd - WAIT_OBJECT_0)
		{
			case 0:
			{
				ResetEvent(pr->handlemass[0]);
				break;
			}
			case 1:
			{
				pr->SetWindowText(_T("Отправитель:"));
				pr->work = true;
				pr->GetDlgItem(IDC_MESSAGE)->EnableWindow(1);
				return 1;
			}
			case 2:
			{
				pr->ReadMessage();
				break;
			}
		}
	}

	return 0;
}

// Обработчики сообщений CSynchronizationDlg
BOOL CSynchronizationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	handlemass[0] = CreateEvent(NULL, TRUE, TRUE, _T("otp_or_pr"));
	handlemass[1] = CreateEvent(NULL, FALSE, FALSE, _T("close"));
	handlemass[2] = CreateEvent(NULL, TRUE, FALSE, _T("text"));

	if (WaitForSingleObject(handlemass[0], 0) == WAIT_OBJECT_0)
	{
		ResetEvent(handlemass[0]);
		SetWindowText(_T("Отправитель:"));
		work = true;
	}
	else
	{
		GetDlgItem(IDC_MESSAGE)->EnableWindow(0);
		SetWindowText(_T("Получатель:"));
		work = false;
		hThread = CreateThread(NULL, 0, MyProc, this, 0, &dwThread);
	}

	hObject = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 2048, TEXT("File_mapping"));

	if (hObject != NULL)
	{
		lpBaseAddress = MapViewOfFile(hObject, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
	}

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CSynchronizationDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CSynchronizationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CSynchronizationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSynchronizationDlg::OnClose()
{

}

void CSynchronizationDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	if (work)
	{
		SetEvent(handlemass[1]);
	}
	else
	{
		TerminateThread(hThread, 0);
		CloseHandle(hThread);
	}

	UnmapViewOfFile(lpBaseAddress);		// отключение от текущего процесса объекта файлового отображения
	CloseHandle(hObject);

	CDialogEx::OnClose();
	CDialogEx::OnCancel();
}
