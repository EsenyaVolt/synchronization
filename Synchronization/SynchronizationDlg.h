﻿
// SynchronizationDlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CSynchronizationDlg
class CSynchronizationDlg : public CDialogEx
{
// Создание
public:
	CSynchronizationDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SYNCHRONIZATION_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString message;

	afx_msg void OnEnChangeMessage();
	afx_msg void OnClose();
	
	void WriteMessage();
	void ReadMessage();

	PVOID lpBaseAddress;
	HANDLE handlemass[3];
	bool work;
	DWORD dwThread;
	HANDLE hThread;
	HANDLE hObject;

	afx_msg void OnBnClickedCancel();
};
